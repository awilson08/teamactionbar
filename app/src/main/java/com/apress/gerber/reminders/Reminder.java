package com.apress.gerber.reminders;

/**
 * Created by Clifton
 * Copyright 12/26/2014.
 */
public class Reminder {

    private int mId;
    private String mContent;
    private int mImportant;
    private String mCategory;

    public Reminder(int id, String content, int important, String category) {
        mId = id;
        mImportant = important;
        mContent = content;
        mCategory = category;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getImportant() {
        return mImportant;
    }

    public void setImportant(int important) {
        mImportant = important;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public String getCategory() {
        return mCategory;
    }
}
