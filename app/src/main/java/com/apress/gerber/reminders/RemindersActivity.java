package com.apress.gerber.reminders;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.app.ActionBar;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;


public class RemindersActivity extends ActionBarActivity {

    private ListView mListView;
    private RemindersDbAdapter mDbAdapter;
    private RemindersSimpleCursorAdapter mCursorAdapter;
    private SpinnerAdapter mSpinnerAdapter;
    private android.support.v7.app.ActionBar actionBar;

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminders);

        mListView = (ListView) findViewById(R.id.reminders_list_view);
        mListView.setDivider(null);
        mDbAdapter = new RemindersDbAdapter(this);
        mDbAdapter.open();

        actionBar = getSupportActionBar();

        if (savedInstanceState == null) {
            //Clean all data
            mDbAdapter.deleteAllReminders();
            //Add some data
            insertSomeReminders();
        }

        Cursor cursor = mDbAdapter.fetchAllReminders();

        //from columns defined in the db
        String[] from = new String[]{
                RemindersDbAdapter.COL_CONTENT
        };

        //to the ids of views in the layout
        int[] to = new int[]{
                R.id.row_text
        };

        mCursorAdapter = new RemindersSimpleCursorAdapter(
                //context
                RemindersActivity.this,
                //the layout of the row
                R.layout.reminders_row,
                //cursor
                cursor,
                //from columns defined in the db
                from,
                //to the ids of views in the layout
                to,
                //flag - not used
                0);


        //the cursorAdapter (controller) is now updating the listView (view) with data from the db (model)
        mListView.setAdapter(mCursorAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int masterPosition, long id) {
                //Toast.makeText(RemindersActivity.this, "clicked " + position, Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder((RemindersActivity.this));
                ListView modeListView = new ListView(RemindersActivity.this);
                String[] modes = new String[]{"Edit Reminder", "Delete Reminder"};
                ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>( //controller
                        //context
                        RemindersActivity.this,

                        //row layout
                        android.R.layout.simple_list_item_1,

                        //ifd of tge text inside the row layout

                        android.R.id.text1,
                        //data
                        modes
                );

                modeListView.setAdapter(modeAdapter);
                builder.setView(modeListView);
                final Dialog dialog = builder.create();
                dialog.show();

                modeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (position == 0) {
                            Toast.makeText(RemindersActivity.this, "edit " + masterPosition, Toast.LENGTH_SHORT).show();
                            int nId = getIdFromPosition(masterPosition);
                            Reminder reminder = mDbAdapter.fetchReminderById(nId);
                            fireCustomDialog(reminder);

                        } else {
                            mDbAdapter.deleteReminderById(getIdFromPosition(masterPosition));
                            mCursorAdapter.changeCursor(mDbAdapter.fetchAllReminders());
                            Toast.makeText(RemindersActivity.this, "delete " + masterPosition, Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                });


            }
        });


        /******  Code for Multiple Selection Functionality ******/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

            mListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                int count = 0;
                ArrayList<Integer> deleted_item = new ArrayList<Integer>();

                @Override
                public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                    if (checked) {
                        count++;
                        mCursorAdapter.setNewSelection(position, checked);
                        deleted_item.add(position);
                    } else {
                        count--;
                        mCursorAdapter.removeSelection(position);
                        deleted_item.remove(deleted_item.indexOf(position));
                    }
                    mode.setTitle(count + " selected");
                }

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.cam_menu, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    if (item.getItemId() == R.id.menu_item_delete_reminder) {
                        for (int position : deleted_item) {
                            mDbAdapter.deleteReminderById((int) mCursorAdapter.getItemId(position));
                        }
                    }
                    mode.finish();
                    deleted_item.clear();
                    count = 0;
                    mCursorAdapter.clearSelection();
                    mCursorAdapter.changeCursor(mDbAdapter.fetchAllReminders());
                    return true;
                }

                //}

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    deleted_item.clear();
                    count = 0;
                    mCursorAdapter.clearSelection();
                    mCursorAdapter.changeCursor(mDbAdapter.fetchAllReminders());
                }
            });
        }

        /***** End of Multiple Choice Selection Functionality *****/


        mSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.action_list,
                R.layout.spinner_dropdown_item);



        /*** Start of drop down menu selector code **/

        android.support.v7.app.ActionBar.OnNavigationListener callback = new android.support.v7.app.ActionBar.OnNavigationListener() {

            String[] items = getResources().getStringArray(R.array.action_list); // List items from res

            @Override
            public boolean onNavigationItemSelected(int position, long id) {
                Cursor cursor = null;
                // Do stuff when navigation item is selected
                switch(items[position]){
                    case "All":
                        // white
                        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
                        cursor = mDbAdapter.fetchAllReminders();
                        mCursorAdapter.changeCursor(cursor);
                        return true;

                    case "Parks and Gardens":
                        // green
                        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.prks_grdn)));
                        cursor = mDbAdapter.fetchRemindersByCategory(items[position]);
                        mCursorAdapter.changeCursor(cursor);
                        return true;

                    case "Entertainment":
                        // yellow
                        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.entertainment)));
                         cursor = mDbAdapter.fetchRemindersByCategory(items[position]);
                        mCursorAdapter.changeCursor(cursor);
                        return true;

                    case "Food and Restaurants":
                        // red
                        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.fd_and_rst)));
                        cursor = mDbAdapter.fetchRemindersByCategory(items[position]);
                        mCursorAdapter.changeCursor(cursor);
                        return true;

                    case "Cultural":
                        // brown
                        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.cultural)));
                        cursor = mDbAdapter.fetchRemindersByCategory(items[position]);
                        mCursorAdapter.changeCursor(cursor);
                        return true;

                    case "Sports":
                        // orange
                        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.sports)));
                        cursor = mDbAdapter.fetchRemindersByCategory(items[position]);
                        mCursorAdapter.changeCursor(cursor);
                        return true;

                    default:
                        cursor = mDbAdapter.fetchRemindersByCategory(items[position]);
                        mCursorAdapter.changeCursor(cursor);
                        return false;
                }
            }

        };

        // Action Bar
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setListNavigationCallbacks(mSpinnerAdapter, callback);

        /*** end ***/


    } //end on create

    private int getIdFromPosition(int position){
        //return ((Reminder) mCursorAdapter.getItem(position)).getId();
        return (int) mCursorAdapter.getItemId(position);
    }


    // creating the floating Context Menu that appears when long clicking
    // on an individual Reminder
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if(v.getId() == R.id.reminders_list_view){
            super.onCreateContextMenu(menu, v, menuInfo);
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_popup, menu);
        }

    }

    private void insertSomeReminders() {
        mDbAdapter.createReminder("Buy Learn Android Studio", true, "Sport");
        mDbAdapter.createReminder("Send Dad birthday gift", false, "Entertainment");
        mDbAdapter.createReminder("Dinner at the Gage on Friday", false, "Cultural");
        mDbAdapter.createReminder("String squash racket", true, "Parks and Gardens");
        mDbAdapter.createReminder("Reminder 1", true, "Entertainment");
        mDbAdapter.createReminder("Another Reminder", true, "Sports");
        mDbAdapter.createReminder("LALALA class", true, "Cultural");
        mDbAdapter.createReminder("Make Dinner", true, "Food and Restaurants");
        mDbAdapter.createReminder("Go buy food", true, "Food and Restaurants");
        mDbAdapter.createReminder("Drive to grocery store", true, "Food and Restaurants");
        mDbAdapter.createReminder("food food food", true, "Food and Restaurants");
        mDbAdapter.createReminder("Go to cultural show", true, "Cultural");
    }


    private void fireCustomDialog(final Reminder reminder){
        final Dialog dialog= new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_custom);

        TextView titleView = (TextView) dialog.findViewById(R.id.custom_title);
        final EditText editCustom = (EditText) dialog.findViewById(R.id.custom_edit_reminder);
        Button commitButton = (Button) dialog.findViewById(R.id.custom_button_commit);
        final CheckBox checkBox = (CheckBox) dialog.findViewById(R.id.custom_check_box);
        LinearLayout rootLayout = (LinearLayout) dialog.findViewById(R.id.custom_root_layout);
        final boolean isEditOperation = (reminder != null);
        final Spinner dropdown = (Spinner) dialog.findViewById(R.id.dropdown_spinner);

        //creating a new spinner adapter for the dropdown (will not have category all to select)
        //dropdown.setAdapter(mSpinnerAdapter);
        SpinnerAdapter dropdownAdapter = ArrayAdapter.createFromResource(this, R.array.dropdown_list, android.R.layout.simple_spinner_dropdown_item);
        dropdown.setAdapter(dropdownAdapter);
        int index;

        // if editing the "to do"
        if (isEditOperation){

            titleView.setText("Edit Reminder");

            checkBox.setChecked(reminder.getImportant() == 1);
            editCustom.setText(reminder.getContent());
            index = findIndexOfCat(reminder.getCategory());
            dropdown.setSelection(index);

            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));

        }

        commitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reminderText = editCustom.getText().toString();
                if (isEditOperation) {
                    Reminder reminderEdited = new Reminder(reminder.getId(),
                            reminderText,
                            checkBox.isChecked() ? 1 : 0,
                            dropdown.getSelectedItem().toString());
                    mDbAdapter.updateReminder(reminderEdited);
                } else {
                    mDbAdapter.createReminder(reminderText, checkBox.isChecked(),
                            dropdown.getSelectedItem().toString());
                }
                mCursorAdapter.changeCursor(mDbAdapter.fetchAllReminders());
                dialog.dismiss();
            }
        });
        Button buttonCancel = (Button) dialog.findViewById(R.id.custom_button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss()
                ;
            }
        });
        dialog.show();

    }

    public int findIndexOfCat(String category){
        switch(category){
            case "Parks and Gardens":
                return 0;
            case "Entertainment":
                return 1;
            case "Food and Restaurants":
                return 2;
            case "Cultural":
                return 3;
            case "Sports":
                return 4;
            default:
                return -1;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reminders, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
                //create new Reminder
                fireCustomDialog(null);
                return true;

            case R.id.action_exit:
                finish();
                return true;

            default:
                return false;
        }
    }
}
